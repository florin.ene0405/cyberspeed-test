using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constantes
{
    public class UIScreenNames
    {
        public const string START_SCREEN = "StartScreen";
        public const string CHOOSE_LAYOUT_SCREEN = "ChooseLayoutScreen";
        public const string IN_GAME_SCREEN = "InGameScreen";
        public const string END_SCREEN = "EndScreen";
    }

    public class Cooldown
    {
        public const int CARD_COOLDOWN = 1; //Seconds until the card will turn back
        public const int GAME_WON_COOLDOWN = 2;
    }

    public class Sounds
    {
        public const string CARD_FLIP = "CARD_FLIP";
        public const string BUTTON_CLICK = "BUTTON_CLICK";
        public const string MATCH_SOUND = "MATCH_SOUND";
        public const string WRONG_SOUND = "WRONG_SOUND";
        public const string WIN_SOUND = "WIN_SOUND";
    }

    public class Save
    {
        public const string SAVE_PATH = "/saveData.json";
    }
}
