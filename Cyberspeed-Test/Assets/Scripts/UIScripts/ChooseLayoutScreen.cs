using TMPro;
using UnityEngine.UI;

public class ChooseLayoutScreen : UIScreen
{
    public Slider RowSlider;
    public Slider ColumSlider;

    public TextMeshProUGUI RowSliderNumber;
    public TextMeshProUGUI ColumSliderNumber;

    public override void Start()
    {
        base.Start();
        RowSlider.onValueChanged.AddListener(SliderUpdate);
        ColumSlider.onValueChanged.AddListener(SliderUpdate);
    }

    private void SliderUpdate(float N)
    {
        if ((RowSlider.value * ColumSlider.value) % 2 != 0)
        {
            if (RowSlider.value > RowSlider.value)
            {
                RowSlider.value = RowSlider.value - 1;
            }
            else
            {
                ColumSlider.value = ColumSlider.value - 1;
            }
        }

        RowSliderNumber.text = RowSlider.value.ToString();
        ColumSliderNumber.text = ColumSlider.value.ToString();
        SoundManager.Instance.PlaySound(Constantes.Sounds.BUTTON_CLICK);
    }

    public void StartGame()
    {
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.IN_GAME_SCREEN);
        GameManager.StartTheGame.Invoke((int)(RowSlider.value * ColumSlider.value));
    }
}
