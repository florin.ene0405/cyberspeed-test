using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameScreen : UIScreen
{
    public GameManager GameManager;
    public TextMeshProUGUI MatchesText;
    public TextMeshProUGUI ComboText;
    public TextMeshProUGUI TurnsText;
    public TextMeshProUGUI ScoreText;

    private void OnEnable()
    {
        GameManager.RefreshTheUI += RefreshUI;
    }

    private void RefreshUI()
    {
        if (!string.Equals(MatchesText.text, GameManager.NumberOfMatches.ToString()))
        {
            MatchesText.transform.DOKill();
            MatchesText.transform.localScale = Vector3.one;
            MatchesText.transform.DOScale(MatchesText.transform.localScale * 1.2f, 0.2f).SetLoops(2,LoopType.Yoyo);
        }
        
        if (!string.Equals(TurnsText.text, GameManager.NumberOfTurns.ToString()))
        {
            TurnsText.transform.DOKill();
            TurnsText.transform.localScale = Vector3.one;
            TurnsText.transform.DOScale(TurnsText.transform.localScale * 1.2f, 0.2f).SetLoops(2, LoopType.Yoyo);
        }
        
        if (!string.Equals(ComboText.text, GameManager.NumberOfTurns.ToString()))
        {
            ComboText.transform.DOKill();
            ComboText.transform.localScale = Vector3.one;
            ComboText.transform.DOScale(ComboText.transform.localScale * 1.2f, 0.2f).SetLoops(2, LoopType.Yoyo);
        }
        
        if (!string.Equals(ScoreText.text, GameManager.NumberOfTurns.ToString()))
        {
            ScoreText.transform.DOKill();
            ScoreText.transform.localScale = Vector3.one;
            ScoreText.transform.DOScale(ScoreText.transform.localScale * 1.2f, 0.2f).SetLoops(2, LoopType.Yoyo);
        }

        ComboText.text = GameManager.Combo.ToString();
        ScoreText.text = GameManager.Score.ToString();
        MatchesText.text = GameManager.NumberOfMatches.ToString();
        TurnsText.text = GameManager.NumberOfTurns.ToString();
    }

    private void OnDisable()
    {
        GameManager.RefreshTheUI -= RefreshUI;
    }
}
