using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : UIScreen
{
    public GameManager GameManager;
    public GameObject ExitPanel;
    public Button ContinueButton;

    private void OnEnable()
    {
        ContinueButton.interactable = GameManager.CanContiuneGame();
    }

    public void ContinueLastGame()
    {
        GameManager.ContinueLastGame();
    }

    public void ShowExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null in " + this);
            return;
        }

        ExitPanel.SetActive(true);
    }

    public void HideExitPanel()
    {
        if (ExitPanel == null)
        {
            Debug.LogError("ExitPanel is null in " + this);
            return;
        }

        ExitPanel.SetActive(false);
    }

    public void RestartTheGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToChooseLayoutScreen()
    {
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.CHOOSE_LAYOUT_SCREEN);
    }

    public void ExitTheGame()
    {
        Application.Quit();
    }
}
