using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    private List<UIScreen> _AllUIScreens = new List<UIScreen>();
    private UIScreen _currentUI;

    #region Singleton
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion

    private void Start()
    {
        LoadFirtsScreen();
    }

    private async void LoadFirtsScreen()
    {
        await Task.Delay(10);

        DeactivateAllUI();
        LoadScreen(Constantes.UIScreenNames.START_SCREEN);
    }

    public void AddScreens(UIScreen newUI)
    {
        _AllUIScreens.Add(newUI);
    }

    public void LoadScreen(string Screen)
    {
        for (int i = 0; i < _AllUIScreens.Count; i++)
        {
            if (string.Equals(_AllUIScreens[i].Name, Screen))
            {
                _AllUIScreens[i].gameObject.SetActive(true);

                if (_currentUI != null)
                {
                    _currentUI.gameObject.SetActive(false);
                }

                _currentUI = _AllUIScreens[i];
                return;
            }
        }

        Debug.LogWarning("There is no Screen named: " + Screen);
    }

    private void DeactivateAllUI()
    {
        _AllUIScreens.ForEach(x => x.gameObject.SetActive(false));
    }
}
