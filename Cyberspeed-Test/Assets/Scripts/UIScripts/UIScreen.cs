using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScreen : MonoBehaviour
{
    public string Name;

    public virtual void Start()
    {
        UIManager.Instance.AddScreens(this);
    }
}
