using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Action<int> StartTheGame;
    public static Action<ImageElement> TurnACard;
    public static Action RefreshTheUI;

    public SaveLoadManager SaveLoadManager;
    public GridManager GridManager;
    public int NumberOfTurns = 0;
    public int NumberOfMatches = 0;
    public int Combo = 0;
    public int Score = 0;


    private List<Sprite> _allSprites = new List<Sprite>();
    private const string ICONS_PATH = "Icons";
    private int _imagesNumber;
    private ImageElement _lastCard;
    private SaveObject _saveObject;

    private void Awake()
    {
        _saveObject = SaveLoadManager.LoadData();
    }

    private void Start()
    {
        _allSprites = Resources.LoadAll<Sprite>(ICONS_PATH).ToList();

        if (_allSprites.Count == 0)
        {
            Debug.LogError("No images found at - " + ICONS_PATH);
        }
        else
        {
            Debug.Log(_allSprites.Count + " images were found");
        }
    }

    public void ContinueLastGame()
    {
        List<Sprite> newList = new List<Sprite>();

        for (int i = 0; i < _saveObject.Elements.Count; i++)
        {
            for (int j = 0; j < _allSprites.Count; j++)
            {
                if (string.Equals( _saveObject.Elements[i].elementName, _allSprites[j].name))
                {
                    newList.Add(_allSprites[j]);
                    _allSprites.RemoveAt(j);
                }
            }
        }

        NumberOfMatches = _saveObject.NuberOfMatches;
        NumberOfTurns = _saveObject.NumberOfTurns;
        Combo = _saveObject.Combo;
        Score = _saveObject.Score;


        GridManager.StartGameFromLoad(_saveObject, newList);
        UIManager.Instance.LoadScreen(Constantes.UIScreenNames.IN_GAME_SCREEN);
        RefreshTheUI.Invoke();
    }

    public bool CanContiuneGame() => _saveObject != null ? true : false;

    private void OnEnable()
    {
        StartTheGame += AllocateRowsAndColums;
        TurnACard += ACardWasTurned;
    }

    public void AllocateRowsAndColums(int number)
    {
        _imagesNumber = number;
        GetRandomIcons();
    }

    private void ACardWasTurned(ImageElement card)
    {
        NumberOfTurns++;

        if (_lastCard != null)
        {
            if (_lastCard.GetID() == card.GetID() && _lastCard.IsCardFaceUp() && card.IsCardFaceUp() && _lastCard != card)
            {
                _lastCard.MatchedTheCard();
                card.MatchedTheCard();
                NumberOfMatches++;
                Combo++;
                SoundManager.Instance.PlaySound(Constantes.Sounds.MATCH_SOUND);
                Score += Combo;
                SaveData();
                CheckForWin();
            }

            if (_lastCard.GetID() != card.GetID() && _lastCard.IsCardFaceUp() && card.IsCardFaceUp() && _lastCard != card)
            {
                SoundManager.Instance.PlaySound(Constantes.Sounds.WRONG_SOUND);
                Combo = 0;
            }

            if (!_lastCard.IsCardFaceUp())
            {
                _lastCard = card;
                Combo = 0;
            }
            else
            {
                _lastCard = null;
            }
        }
        else
        {
            _lastCard = card;
        }

        RefreshTheUI.Invoke();
    }

    private async void CheckForWin()
    {
        if (GridManager.IsGameWon())
        {
            await Task.Delay(Constantes.Cooldown.GAME_WON_COOLDOWN * 1000);

            UIManager.Instance.LoadScreen(Constantes.UIScreenNames.END_SCREEN);
            SoundManager.Instance.PlaySound(Constantes.Sounds.WIN_SOUND);
            SaveLoadManager.DeleteData();
        }
    }

    private void GetRandomIcons()
    {
        int random;
        int removeNumber = _imagesNumber / 2;
        List<Sprite> newImages = new List<Sprite>();

        for (int i = 0; i < removeNumber; i++)
        {
            random = UnityEngine.Random.Range(0, _allSprites.Count);
            newImages.Add(_allSprites[random]);
            _allSprites.RemoveAt(random);
        }

        GridManager.GenerateGrid(newImages);

        SaveData();
    }

    private void OnDisable()
    {
        StartTheGame -= AllocateRowsAndColums;
        TurnACard -= ACardWasTurned;
    }

    private void SaveData()
    {
        SaveLoadManager.DeleteData();

        var images = GridManager.GetAllImagesButtons();
        SaveObject save = new SaveObject();

        save.Combo = Combo;
        save.Score = Score;
        save.NumberOfTurns = NumberOfTurns;
        save.NuberOfMatches = NumberOfMatches;

        foreach (var item in images)
        {
            ObjetData element = new ObjetData(item.Name, item.WasMatche());
            save.Elements.Add(element);
        }

        SaveLoadManager.SaveData(save);
    }
}
