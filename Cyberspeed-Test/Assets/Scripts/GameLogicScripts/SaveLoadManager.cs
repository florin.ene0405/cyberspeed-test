using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveLoadManager : MonoBehaviour
{
    private string savePath;

    private void Awake()
    {
        savePath = Application.persistentDataPath + Constantes.Save.SAVE_PATH;
    }

    public void SaveData(SaveObject data)
    {
        string jsonData = JsonUtility.ToJson(data);
        File.WriteAllText(savePath, jsonData);
    }

    public SaveObject LoadData()
    {
        if (File.Exists(savePath))
        {
            string jsonData = File.ReadAllText(savePath);
            return JsonUtility.FromJson<SaveObject>(jsonData);
        }
        else
        {
            Debug.Log("Save file not found!");
            return null;
        }
    }

    public void DeleteData()
    {
        if (File.Exists(savePath))
        {
           File.Delete(savePath);
        }
    }
}
