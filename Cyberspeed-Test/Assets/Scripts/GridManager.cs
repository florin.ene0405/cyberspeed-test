using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    public ImageElement ImageElementPrefab;
    public GridLayoutGroup GridLayout;

    private List<ImageElement> _allImageElement = new List<ImageElement>();
    private int ySpacing = 20;
    private SaveObject _saveObject;

    public void GenerateGrid(List<Sprite> sprites)
    {
        int id = 0;
        foreach (var sprite in sprites)
        {
            var newImage = Instantiate<ImageElement>(ImageElementPrefab, transform);
            var newImageCoppy = Instantiate<ImageElement>(ImageElementPrefab, transform);
            newImage.Initialize(sprite, id);
            newImageCoppy.Initialize(sprite, id);
            _allImageElement.Add(newImage);
            _allImageElement.Add(newImageCoppy);
            id++;
        }

        GridLayout.spacing = new Vector2(0, ySpacing);

        float height = GetComponent<RectTransform>().rect.height;
        float rowNumber = Mathf.Sqrt(_allImageElement.Count);

        float cellSize = (height / rowNumber - ySpacing) * 0.9f;

        GridLayout.cellSize = new Vector2(cellSize, cellSize);

        GridLayout.constraintCount = (int)rowNumber;

        gameObject.SetActive(true);

        int randon;

        foreach (var item in _allImageElement)
        {
            randon = Random.Range(0, _allImageElement.Count);

            item.transform.SetSiblingIndex(randon);
        }
    }

    public void StartGameFromLoad(SaveObject save, List<Sprite> newList)
    {
        _saveObject = save;
        GenerateGrid(newList);

        foreach (var item in _saveObject.Elements)
        {
            for (int i = 0; i < 2; i++)
            {
                var img = GetImageElementByName(item.elementName);
                if (img != null && item.elementValue)
                {
                    img.MatchedTheCard(true);
                    _allImageElement.Remove(img);
                }
            }
        }
    }


    private ImageElement GetImageElementByName(string name)
    {
        foreach (var item in _allImageElement)
        {
            if (string.Equals(name, item.Name))
            {
                return item;
            }
        }

        return null;
    }

    public bool IsGameWon()
    {
        int nr = 0;

        _allImageElement.ForEach(x =>
        {
            if (!x.WasMatche())
            {
                nr++;
            }
        });

        return nr == 0 ? true : false;
    }

    public List<ImageElement> GetAllImagesButtons() => _allImageElement;
}
