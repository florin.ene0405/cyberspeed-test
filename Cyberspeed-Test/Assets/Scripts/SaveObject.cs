using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveObject 
{
    public List<ObjetData> Elements;
    public int Score;
    public int NumberOfTurns;
    public int Combo;
    public int NuberOfMatches;

    public SaveObject()
    {
        Elements = new List<ObjetData>();
    }
}

[Serializable]
public class ObjetData
{
    public string elementName;
    public bool elementValue;

    public ObjetData(string name, bool value)
    {
        elementName = name;
        elementValue = value;
    }
}
