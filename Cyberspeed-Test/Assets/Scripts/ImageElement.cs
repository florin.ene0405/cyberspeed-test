using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ImageElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public string Name;
    public Image BackImage;
    public Image FrontImage;
    public Image MyImage;
    public GameObject HoverEffect;

    private bool _wasMatched = false;
    private bool _isCardFaceUp = false;
    private int _iD;


    public void Initialize(Sprite sprite, int id)
    {
        MyImage.sprite = sprite;
        HoverEffect.SetActive(false);
        _iD = id;
        Name = sprite.name;
        HideImage();
    }

    public int GetID() => _iD;
    public bool WasMatche() => _wasMatched;
    public bool IsCardFaceUp() => _isCardFaceUp;

    private async void HideImage()
    {
        await Task.Delay(1000);
        FrontImage.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_wasMatched || _isCardFaceUp)
        {
            return;
        }

        FrontImage.gameObject.SetActive(true);
        HoverEffect.SetActive(false);
        SoundManager.Instance.PlaySound(Constantes.Sounds.CARD_FLIP);
        _isCardFaceUp = true;
        Anim();
        GameManager.TurnACard.Invoke(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_wasMatched || _isCardFaceUp)
        {
            return;
        }

        HoverEffect.SetActive(true);
    }

    public async void MatchedTheCard(bool skipAnimation = false)
    {
        _wasMatched = true;
        if (!skipAnimation)
        {
            await Task.Delay(Constantes.Cooldown.CARD_COOLDOWN * 1000);
        }

        FrontImage.gameObject.SetActive(false);
        BackImage.gameObject.SetActive(false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HoverEffect.SetActive(false);
    }

    private async void Anim()
    {
        await Task.Delay(Constantes.Cooldown.CARD_COOLDOWN * 1000);
        if (!_wasMatched)
        {
            FrontImage.gameObject.SetActive(false);
            _isCardFaceUp = false;
        }
    }
}
