using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioSource AudioSource;
    public AudioClip CardFlip;
    public AudioClip ButtonClick;
    public AudioClip MatchSound;
    public AudioClip WrongSound;
    public AudioClip WinSound;
    public AudioClip BackgroindSound;

    #region Singleton
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion

    private void Start()
    {
        AudioSource.clip = BackgroindSound;
        AudioSource.Play();
    }

    public void PlaySound(string sound)
    {
        AudioClip clip;

        switch (sound)
        {
            case  Constantes.Sounds.CARD_FLIP:
                AudioSource.PlayClipAtPoint(CardFlip, Vector3.zero);
                break;
            case Constantes.Sounds.BUTTON_CLICK:
                AudioSource.PlayClipAtPoint(ButtonClick, Vector3.zero);
                break;
            case Constantes.Sounds.MATCH_SOUND:
                AudioSource.PlayClipAtPoint(MatchSound, Vector3.zero);
                break;
            case Constantes.Sounds.WRONG_SOUND:
                AudioSource.PlayClipAtPoint(WrongSound, Vector3.zero);
                break;
            case Constantes.Sounds.WIN_SOUND:
                AudioSource.PlayClipAtPoint(WinSound, Vector3.zero);
                AudioSource.Stop();
                break;
            default:
                break;
        }
    }
}

public enum dd
{
    d,a,aw,asd,ad
}